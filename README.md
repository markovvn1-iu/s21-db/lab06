# Lab 06

### Task 1

See `ex1.sql`

EXPLAIN ANALYZE of task 1:

![ex1](.gitlab/ex1_explain_analyze.png "Ex1")

### Task 2

See `ex2.sql`