/*
	The most expensive step of this queries is selective search in rental
	table under condition (return_date IS null). It takes 59% of time. We can
	speed up this part of query by creating indices on column return_date
*/

SELECT * FROM film as f
WHERE (f.rating = 'R' OR f.rating = 'PG-13') AND

film_id NOT IN(SELECT film_id FROM rental AS r
LEFT JOIN inventory AS i
ON r.inventory_id = i.inventory_id
WHERE return_date IS null) AND

film_id IN(SELECT film_id FROM film_category fcat
INNER JOIN category AS cat
ON cat.category_id = fcat.category_id
WHERE cat.name = 'Horror' OR cat.name = 'Sci-Fi')