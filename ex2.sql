-- Create temporary table to speed up computing
-- This table will store sums of all payments of staff per store with city id
CREATE TEMPORARY TABLE payment_per_store ON COMMIT DROP AS
SELECT res.store_id, address.city_id, res.payment FROM (
	SELECT staff.store_id, SUM(payment_per_staff.payment) AS payment FROM (
		SELECT pt.staff_id, SUM(pt.amount) as payment
		FROM payment AS pt
		GROUP BY pt.staff_id) AS payment_per_staff
	INNER JOIN staff
	ON staff.staff_id = payment_per_staff.staff_id
	GROUP BY staff.store_id) AS res
INNER JOIN store
ON store.store_id = res.store_id
INNER JOIN address
ON address.address_id = store.address_id;

-- Select best store per city
SELECT city.city_id, pps3.store_id FROM city
LEFT JOIN payment_per_store AS pps3
ON pps3.city_id = city.city_id AND pps3.payment = (SELECT MAX(pps2.payment) FROM payment_per_store AS pps2 WHERE pps2.city_id = city.city_id)
ORDER BY city.city_id ASC